import React from "react";
import COMPONENT_NAME from "./COMPONENT_NAME";

export default {
  component: COMPONENT_NAME,
  title: "COMPONENT_NAME",
};

const Template = (args) => <COMPONENT_NAME {...args} />;

export const Default = Template.bind({});
Default.args = {};
